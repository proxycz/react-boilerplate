# DM_ React Boilerplate

A template of README best practices to make your README simple to understand and easy to use.

## Table of Contents

- [Installation](#installation)
- [Config](#config)
- [Capability](#capability)
- [Support](#support)
- [License](#license)

## Installation

Download repo [here](https://bitbucket.org/proxycz/react-boilerplate/src/master/):
```sh
cd your_folder
npm install
npm start
```

## Config

Config section is located in webpack.js files at the bottom:

```sh
apiUrl: 'API_URL',
pusherKey: 'PUSHER_API_KEY',
pusherChannel: 'PUSHER_CHANNEL'
```

To include config variables in .js files simply add:
```sh
import config from 'config'
```

## Capability

- Login to dashboard capability
- Register
- Alert capabilities
- Pusher context provider
- Sass styling


## Support

Please [open an issue](https://bitbucket.org/proxycz/react-boilerplate/issues/new) for support.

### License

DM_ React Boilerplate is [MIT licensed](./LICENSE).
