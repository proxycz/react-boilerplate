import * as userConstants from '../constants/user'

const initialState = null

export function user (state = initialState, action) {
  switch (action.type) {
    case userConstants.USER_LOGIN:
      return action.user
    case userConstants.USER_UPDATE:
      return action.user
    case userConstants.USER_REAUTHORIZE:
      return action.user
    case userConstants.USER_LOGOUT:
      return null
    default:
      return state
  }
}
