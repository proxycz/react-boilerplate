import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { user } from './user'
import { alert } from './alert'

const rootReducer = combineReducers({
  user,
  alert,
  form: formReducer
})

export default rootReducer
