import React, { useState } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { Form, Button, Input } from 'semantic-ui-react'
import classNames from 'classnames'
import PropTypes from 'prop-types'

Register.propTypes = {
  signUp: PropTypes.func,
  user: PropTypes.object,
  history: PropTypes.object
}
export default function Register (props) {
  const [username, setUsername] = useState('')

  const [error, setError] = useState(false)

  const [submitting, setSubmitting] = useState(false)
  const { signUp, user, history } = props

  function handleRegister (e) {
    e.preventDefault()
    const regexEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    if (username.length === 0 || !regexEmail.test(username)) {
      setError(true)
    } else {
      setSubmitting(true)
      if (username) {
        signUp(username).then((response) => {
          if (response) {
            setSubmitting(false)
          } else {
            setSubmitting(false)
          }
        })
      }
    }
  }
  function handleChange (e) {
    const { value } = e.target
    setUsername(value)
    setError(false)
  }
  return (user ? <Redirect to='/' />
    : <div className='content'>
      <div className='sign-gate'>
        <Form name='form' className='sign-gate-content' onSubmit={handleRegister}>
          <h2>Sign Up</h2>
          <Form.Field>
            <label>Your email</label>
            <Input error={error} fluid type='email' name='username' value={username} onChange={handleChange} className={classNames({ 'filled': username.length > 0 })} />
            {error ? <div className='form-error'>Oops. This doesn’t look right.</div> : ''}
          </Form.Field>
          <Button primary fluid loading={submitting}>
            Continue
          </Button>
          <p className='color-gray-400'>By clicking “Continue” above, you acknowledge that you have read and understood, and agree to Draft’s <a href='/' className='link gray' target='_blank'>Terms and Conditions</a> and <a href='/' className='link gray' target='_blank'>Privacy Policy</a>.</p>
        </Form>
      </div>
    </div>
  )
}
