import Register from './Register'
import { connect } from 'react-redux'
import { signUp } from '../../../actions/user'

function mapStateToProps (state) {
  const { user } = state
  return {
    user
  }
}
const mapDispatchToProps = dispatch => {
  return {
    signUp (username) {
      return dispatch(signUp(username))
    }
  }
}
const connectedRegister = connect(mapStateToProps, mapDispatchToProps)(Register)
export { connectedRegister as Register }
