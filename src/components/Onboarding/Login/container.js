import Login from './Login'
import { connect } from 'react-redux'
import { login } from '../../../actions/user'

function mapStateToProps (state) {
  const { user } = state
  return {
    user
  }
}
const mapDispatchToProps = dispatch => {
  return {
    login (email) {
      return dispatch(login(email))
    }
  }
}
const connectedLogin = connect(mapStateToProps, mapDispatchToProps)(Login)
export { connectedLogin as Login }
