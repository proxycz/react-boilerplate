import React, { useState } from 'react'
import { Redirect } from 'react-router-dom'
import { Form, Button, Input } from 'semantic-ui-react'
import classNames from 'classnames'
import PropTypes from 'prop-types'

Login.propTypes = {
  login: PropTypes.func,
  user: PropTypes.object
}
export default function Login (props) {
  const [username, setUsername] = useState('')
  const [error, setError] = useState(false)
  const [submitting, setSubmitting] = useState(false)
  const { login, user } = props

  function handleLogin (e) {
    e.preventDefault()
    const regexEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    if (username.length === 0 || !regexEmail.test(username)) {
      setError(true)
    } else {
      setSubmitting(true)
      if (username) {
        login(username).then((response) => {
          if (response) {
            setSubmitting(false)
          } else {
            setSubmitting(false)
          }
        })
      }
    }
  }
  function handleChange (e) {
    const { value } = e.target
    setUsername(value)
    setError(false)
  }
  return (user ? <Redirect to='/' />
    : <div className='content'>
      <div className='sign-gate'>
        <Form name='form' className='sign-gate-content' onSubmit={handleLogin}>
          <h2>Sign In</h2>
          <Form.Field>
            <label>Your email</label>
            <Input error={error} fluid type='email' name='username' value={username} onChange={handleChange} className={classNames({ 'filled': username.length > 0 })} />
            {error ? <div className='form-error'>Oops. This doesn’t look right.</div> : ''}
          </Form.Field>
          <Button primary fluid loading={submitting}>
            Continue
          </Button>
        </Form>
      </div>
    </div>
  )
}
