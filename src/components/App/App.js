import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import { Switch, Router, Route } from 'react-router-dom'
import { history } from '../../helpers/history'
import { PrivateRoute } from '../PrivateRoute'
import { Dashboard } from '../Dashboard'
import { Login } from '../Onboarding/Login'
import { Register } from '../Onboarding/Register'
import '../../sass/app.scss'
import { useAlert } from 'react-alert'

App.propTypes = {
  alert: PropTypes.object,
  clearAlert: PropTypes.func
}
export default function App (props) {
  const alertModal = useAlert()
  const { alert, clearAlert } = props

  useEffect(() => {
    if (alert) {
      alertModal.show(alert.message, {
        position: 'top left',
        timeout: 3000,
        type: alert.type,
        onClose: () => {
          if (alert) {
            clearAlert()
          }
        }
      })
    }
  }, [alert])

  return (
    <Router history={history}>
      <Switch>
        <Route path='/login' component={Login} />
        <Route path='/register' component={Register} />
        <PrivateRoute path='/' component={Dashboard} />
      </Switch>
    </Router>
  )
}
