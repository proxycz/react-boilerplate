import App from './App'
import { connect } from 'react-redux'
import { clear } from '../../actions/alert'

const mapStateToProps = state => {
  const { alert } = state
  return {
    alert
  }
}
const mapDispatchToProps = dispatch => {
  return {
    clearAlert () {
      dispatch(clear())
    }
  }
}

const connectedApp = connect(mapStateToProps, mapDispatchToProps)(App)
export { connectedApp as App }
