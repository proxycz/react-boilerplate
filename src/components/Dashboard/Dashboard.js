import React, { useEffect } from 'react'
//import { Route } from 'react-router-dom'  //Uncomment with use of routes
import { Loader } from 'semantic-ui-react'
import PusherContext from '../../contexts/PusherContext'

import Pusher from 'pusher-js'
import config from 'config'

const pusher = new Pusher(config.pusherKey, {
  cluster: 'eu',
  forceTLS: true
})
var channel = pusher.subscribe(config.pusherChannel)

export default function Dashboard (props) {
  const tokenUser = JSON.parse(localStorage.getItem('user'))
  const { user, reauthorize } = props
  useEffect(() => {
    if (!user && tokenUser) {
      reauthorize(tokenUser)
    }
  }, [])

  let dashboard = <Loader active />
  if (user) {
    dashboard = <PusherContext.Provider value={channel}>
      <div className='dashboard'>
      Dashboard
        {/*<Route exact path='/' component={} />*/}
      </div>
    </PusherContext.Provider>
  }
  return dashboard
}
