import Dashboard from './Dashboard'
import { connect } from 'react-redux'
import { reauthorize } from '../../actions/user'

function mapStateToProps (state) {
  const { user } = state
  return {
    user
  }
}
const mapDispatchToProps = dispatch => {
  return {
    reauthorize (...args) {
      return dispatch(reauthorize(...args))
    }
  }
}
const connectedDashboard = connect(mapStateToProps, mapDispatchToProps)(Dashboard)
export { connectedDashboard as Dashboard }
