export function authHeader () {
  let user = JSON.parse(localStorage.getItem('user'))

  if (user) {
    return { 'Authorization': 'Bearer ' + user.token, 'Content-Type': 'application/json' }
  } else {
    return {}
  }
}
