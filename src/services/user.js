import config from 'config'
import { authHeader } from '../helpers/authHeader'
import { handleResponse } from './main'

export const login = (username) => {
  // Remove this
  if (username === 'admin@admin.com') {
    const data = {
      user:{
        email: 'admin@admin.com',
        token: '1hj35jhkj234k2j34hk23j4h',
        fullname: 'John Doe'
      }
    }
    localStorage.setItem('user', JSON.stringify(data.user))
    return Promise.resolve({ data: data.user })
  }
  //
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ username })
  }

  return fetch(`${config.apiUrl}/login`, requestOptions)
    .then(handleResponse)
    .then(response => {
      if (response) {
        localStorage.setItem('user', JSON.stringify(response.data.user))
      }
      return response.data.user
    })
}
export const signUp = (username) => {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ username })
  }

  return fetch(`${config.apiUrl}/signUp`, requestOptions)
    .then(handleResponse)
    .then(response => {
      return response
    })
}

export const reauthorize = (data) => {
  // Remove this
  if (data.email === 'admin@admin.com') {
    const data = {
      user:{
        email: 'admin@admin.com',
        token: '1hj35jhkj234k2j34hk23j4h',
        fullname: 'John Doe'
      }
    }
    localStorage.setItem('user', JSON.stringify(data.user))
    return Promise.resolve({ data: data.user })
  }
  //
  const requestOptions = {
    method: 'POST',
    headers: authHeader(),
    body: JSON.stringify(data)
  }

  return fetch(`${config.apiUrl}/reauthorize`, requestOptions)
    .then(handleResponse)
    .then(response => {
      return response
    })
}

export const logout = () => {
  const requestOptions = {
    method: 'POST',
    headers: authHeader()
  }

  return fetch(`${config.apiUrl}/logout`, requestOptions).then(handleResponse).then(response => {
    return response
  })
}
