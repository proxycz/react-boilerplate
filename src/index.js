import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { store } from './helpers/store'
import { App } from './components/App'
import { Provider as AlertProvider } from 'react-alert'

const AlertTemplate = ({ style, options, message, close }) => (
  <div className={'alert ' + options.type} style={style}>
    {message}
  </div>
)

render(
  <Provider store={store}>
    <AlertProvider template={AlertTemplate}>
      <App />
    </AlertProvider>
  </Provider>,
  document.getElementById('app')
)
