import * as alertConstants from '../constants/alert'

export const success = (message) => {
  return {
    type: alertConstants.SUCCESS,
    message
  }
}

export const info = (message) => {
  return {
    type: alertConstants.INFO,
    message
  }
}

export const error = (message) => {
  return {
    type: alertConstants.ERROR,
    message
  }
}

export const clear = () => {
  return {
    type: alertConstants.CLEAR
  }
}
