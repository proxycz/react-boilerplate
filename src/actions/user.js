import * as userService from '../services/user'
import { error as errorAlert, info as infoAlert, success as successAlert } from './alert'
import { history } from '../helpers/history'
import * as userConstants from '../constants/user'

export const login = (username) => {
  return dispatch => {
    return userService.login(username)
      .then(
        response => {
          dispatch({ type: userConstants.USER_LOGIN, user: response.data })
          return true
        },
        error => {
          dispatch(errorAlert(error.toString()))
          return false
        }
      )
  }
}

export const signUp = (username) => {
  return dispatch => {
    return userService.signUp(username)
      .then(
        response => {
          return true
        },
        error => {
          dispatch(errorAlert(error.toString()))
          return false
        }
      )
  }
}

export const logout = () => {
  return dispatch => {
    return userService.logout()
      .then(
        response => {
          localStorage.removeItem('user')
          dispatch({ type: userConstants.USER_LOGOUT })
          history.push('/login')
        }
      )
  }
}
export const updateUser = (user, id) => {
  return dispatch => {
    return userService.updateUser(user, id)
      .then(
        response => {
          dispatch(infoAlert(response.message.toString()))
          dispatch({ type: userConstants.USER_UPDATE, user: response.data })
          return true
        },
        error => {
          dispatch(errorAlert(error.toString()))
          return false
        }
      )
  }
}

export const reauthorize = (user) => {
  return dispatch => {
    return userService.reauthorize(user)
      .then(
        response => {
          dispatch({ type: userConstants.USER_REAUTHORIZE, user: response.data })
          return true
        },
        error => {
          dispatch(errorAlert(error.toString()))
          return false
        }
      )
  }
}
